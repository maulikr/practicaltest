<?php declare (strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use App\Models\Question;
use App\Models\Exam;

class ImportController extends Controller
{
    
    public function index(Request $request) {

        return view('welcome');
    }

    public function importFile(Request $request) {
        $data = array();
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:xlsx|max:2048'
        ]);
        if ($validator->fails()) {
            $data['success'] = 101;
            $data['message'] = $validator->errors()->first('file');// Error response

        }else{
             if($request->file('file')) {
                $file = $request->file('file');
                $filename = time().'_'.$file->getClientOriginalName();
                // File extension
                $extension = $file->getClientOriginalExtension();
                // File upload location
                $location = 'upload';
                // Upload file
                $file->move($location,$filename);
                // File path
                $filepath = url($location.'/'.$filename);

                $reader = new Xlsx();
                $reader->setReadDataOnly(true);
                // $reader->setLoadSheetsOnly(['Sheet1']);
                // Load selected area only.
                $spreadsheet = $reader->load($location.'/'.$filename);
                $data = $this->checkAndImportData($spreadsheet,'Sheet1');
             } else{
                 // Response
                 $data['success'] = 101;
                 $data['message'] = 'File not uploaded.'; 
             }
        }

        return response()->json($data);
    }

     /**
     * Validate imported file and insert in to db.
     *
     * @param $spreadsheet , $sheetnames
     * @return mixed
     */

    public function checkAndImportData($spreadsheet, $sheetnames)
    {
        $activeSheet = $spreadsheet->getSheetByName($sheetnames);

        $activeSheets = [
                'rules'                  => [
                    'A' => ['required'], // username
                    'B' => ['required'], // question
                    'G' => ['required'], // Answer
                ],
                'messages'               => [
                    'A.required' => 'The username  field is required.',

                    'B.required' => 'The question field is required.',

                    'G.required' => 'The option field is required.',
                ],
                'skipRows'               => [1],
                'skipColumns'            => 'G',
                'optionsStartFrom'       => 'C',
                'minimumRequiredOptions' => 2,
        ];

        $excelData           = [];
        $excelFileAsError = [];
        $skipRows = $activeSheets['skipRows'];
        $skipColumns = $activeSheets['skipColumns'];
        $processData = $this->getExcelData($activeSheet, $skipRows ,$skipColumns  );
        $excelData = $processData['excelData'];
        $spreadsheet->setActiveSheetIndexByName($sheetnames);

        // Check excel file validation as per rule
        foreach ($excelData as $currentRowNumber => $rowData) {
            $rules    = $activeSheets['rules'];
            $messages = $activeSheets['messages'];

            $validator = Validator::make($rowData, $rules, $messages);

            if ($validator->fails()) {
                foreach ($validator->getMessageBag()->getMessages() as $columnName => $errorMessage) {
                    $errorAtWhichCell = $columnName . $currentRowNumber;
                    $error            = implode(',', $errorMessage);
                    $color            = new Color();
                    $spreadsheet->getActiveSheet()
                        ->getComment($errorAtWhichCell)
                        ->getText()
                        ->createTextRun($error)
                        ->getFont()
                        ->setBold(true)
                        ->setColor($color->setARGB(Color::COLOR_RED));

                    // Set back ground color
                    $spreadsheet->getActiveSheet()->getStyle($errorAtWhichCell)->getFill()
                        ->setFillType(Fill::FILL_SOLID)
                        ->getStartColor()->setARGB(Color::COLOR_RED);
                    $excelFileAsError[$currentRowNumber][$columnName] = $error;
                }
            }

            $choiceAvailableOptions = 0;
            $minimumOptionsRequired = $activeSheets['minimumRequiredOptions'];
            $currentColumnValue = $activeSheets['optionsStartFrom'];
            $values             = [];
            foreach ($rowData as $columnName => $value) {
                $currentColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($currentColumnValue);
                $skipColumnIndex    = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($columnName);
                if ($currentColumnIndex > $skipColumnIndex) {
                    // skip unwanted column if not set.
                    continue;
                }
                if (isset($value) && !empty($value) && strlen((string) $value) >= 1) {
                    $choiceAvailableOptions = $choiceAvailableOptions + 1;
                    $values[$columnName]    = $value;
                }
            }

            if ($choiceAvailableOptions < $minimumOptionsRequired) {
                // Minum option is not available to add as error message.
                $error = 'At least two options are required for this question.';
                $color = new Color();

                $newColumnName = $options['optionsStartFrom'];
                $myCell        = [];
                for ($i = 0; $i <= 6; $i++) {
                    $errorAtWhichCell = $newColumnName . $currentRowNumber;
                    $myCell[]         = $errorAtWhichCell;
                    $spreadsheet->getActiveSheet()
                        ->getComment($errorAtWhichCell)
                        ->getText()
                        ->createTextRun($error)
                        ->getFont()
                        ->setBold(true)
                        ->setColor($color->setARGB(Color::COLOR_RED));

                    // Set back ground color
                    $spreadsheet->getActiveSheet()->getStyle($errorAtWhichCell)->getFill()
                        ->setFillType(Fill::FILL_SOLID)
                        ->getStartColor()->setARGB(Color::COLOR_RED);
                    $excelFileAsError[$currentRowNumber][$columnName] = $error;
                    $newColumnName++;
                }
            }
        }

        $data = [];
        if (!empty($excelFileAsError) && count($excelFileAsError) > 0) {

            // generate new file with validation messsages if there is any validation error found for cell
            $errorFilename = time()."_error.xlsx";
            $tmpCorrectedFileName = base_path()."/public/upload/".$errorFilename;
            $writer = IOFactory::createWriter($spreadsheet, "Xlsx");
            $writer->save($tmpCorrectedFileName);

            $data['success'] = 102;
            $data['message'] = 'Required field Error.'; 
            $data['data'] = [
                "filepath" => url('upload/'.$errorFilename),
                "errorFilename" => $errorFilename 
            ]; 

        } else { // If there is no error found than start import into database.
            $processDataToStore = [];

            foreach ($excelData as $key => $value) {
                $processDataToStore[$value['A']][] = $value;
            }

            foreach ($processDataToStore as $key => $value) {
                $examObj = Exam::updateOrCreate([
                    "title" => $key
                ]);
                $examQuestionIds = [];
                foreach ($value as $key1 => $value1) {
                    $questionObj = Question::updateOrCreate([
                        "title" => $value1['B']
                    ],[
                        "option1" => $value1['C'],
                        "option2" => $value1['D'],
                        "option3" => $value1['E'],
                        "option4" => $value1['F'],
                        "answer" => $value1['G'],
                    ]);
                    $examQuestionIds[] = $questionObj->id;
                }
                $examObj->questions()->sync($examQuestionIds, false);
            }

            $data['success'] = 200;
            $data['message'] = 'Data imported successfully.'; 
        }
        return $data;
    }

        /**
     * To array of excel sheet.
     *
     * @param $activeSheet
     * @return mixed
     */
    public function getExcelData($activeSheet, $skipRows , $skipColumns)
    {
        $excelData = [];
        // Extract data from excel sheet.
        foreach ($activeSheet->getRowIterator() as $key => $cellIterator) {
            $currentRowWithCellId         = [];
            $currentRowNumber             = $cellIterator->getRowIndex();
            if (in_array($currentRowNumber, $skipRows)) {
                continue;
            }
            $isEmptyRow  = false;
            $totalColumn = 0;
            foreach ($cellIterator->getCellIterator() as $cell) {
                $currentColumn      = $cell->getColumn();
                $cellValue          = $cell->getValue();

                $currentColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($currentColumn);
                $skipColumnIndex    = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($skipColumns);
                if ($currentColumnIndex > $skipColumnIndex) {
                    continue;
                }

                $currentRowWithCellId[$currentRowNumber][$currentColumn] = $cellValue;
                $excelData[$currentRowNumber][$currentColumn]            = $cellValue;
                $totalColumn++;
            }
            // Check weather the current row is null.
            $isEmptyCellCount = 0;
            foreach ($excelData[$currentRowNumber] as $rowIdNumber => $valueData) {
                if (empty($valueData)) {
                    $isEmptyCellCount = $isEmptyCellCount + 1;
                }
            }
            if ($isEmptyCellCount === $totalColumn) {
                // If full row is empty then remove it.
                unset($excelData[$currentRowNumber]);
            }
        }
        return ['excelData' => $excelData];
    }

}
