<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ExamQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_question', function (Blueprint $table) {
            $table->bigIncrements('id')->comment("primary key of the current table");
            $table->unsignedBigInteger('exam_id')->unsigned()->index('exam_id')->comment('id from exam table');
            $table->unsignedBigInteger('question_id')->unsigned()->index('question_id')->comment('id from question table');
            $table->timestamp('created_at')->useCurrent()->comment("date and time when record is created");
            $table->timestamp('updated_at')->useCurrent()->comment("date and time when record is updated");

            $table->foreign('exam_id')
                    ->references('id')
                    ->on('exam')
                    ->onUpdate('CASCADE')
                    ->onDelete('CASCADE');

            $table->foreign('question_id')
                    ->references('id')
                    ->on('question')
                    ->onUpdate('CASCADE')
                    ->onDelete('CASCADE');

         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('exam_question');
        Schema::enableForeignKeyConstraints();
    }
}
