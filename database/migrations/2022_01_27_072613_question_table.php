<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class QuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question', function (Blueprint $table) {
            $table->bigIncrements('id')->comment("primary key of the current table");
            $table->text('title', 65535)->comment('title of question');
            $table->string('option1',255)->nullable();
            $table->string('option2',255)->nullable();
            $table->string('option3',255)->nullable();
            $table->string('option4',255)->nullable();
            $table->string('answer',255)->nullable();
            $table->timestamp('created_at')->useCurrent()->comment("date and time when record is created");
            $table->timestamp('updated_at')->useCurrent()->comment("date and time when record is updated");
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('question');
        Schema::enableForeignKeyConstraints();
    }
}
